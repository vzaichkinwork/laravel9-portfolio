<!-- ====== About Section Start -->
<section id="about" class="dark:bg-slate-700 pt-20 lg:pt-[120px] pb-12 lg:pb-[90px] overflow-hidden">
    <div class="container">
        <div class="flex flex-wrap justify-between items-center -mx-4">
            <div class="w-full lg:w-6/12 px-4">
                <div class="flex items-center -mx-3 sm:-mx-4">
                    <div class="w-full xl:w-1/2 px-3 sm:px-4">
                        <div class="py-3 sm:py-4">
                            <img
                                src="{{ url('/img/me1.png') }}"
                                alt=""
                                class="rounded-2xl w-full"
                            />
                        </div>
                        <div class="py-3 sm:py-4">
                            <img
                                src="{{ url('/img/me3.png') }}"
                                alt=""
                                class="rounded-2xl w-full"
                            />
                        </div>
                    </div>
                    <div class="w-full xl:w-1/2 px-3 sm:px-4">
                        <div class="my-4 relative z-10">
                            <img
                                src="{{ url('/img/me2.png') }}"
                                alt=""
                                class="rounded-2xl w-full"
                            />
                            <x-about-dots></x-about-dots>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-full lg:w-1/2 xl:w-5/12 px-4">
                <div class="mt-10 lg:mt-0">
          <span class="font-semibold text-lg text-primary mb-2 block">
             <blockquote class="text-sm text-gray-500 italic py-2 px-3 border-l-4 border-yellow-300">
                 "Everything is achievable with hard work"
             </blockquote>
          </span>
                    <h2 class="font-bold text-3xl sm:text-4xl dark:text-gray-200 mb-8">
                        About Me
                    </h2>
                    <p class="text-base dark:text-gray-400 mb-8">
                        I have been working as a software developer at different kinds of commercial projects. <br />
                        Ready to use the acquired knowledge, as well as learn new things. <br />
                        Mostly I do backend with <span class="text-yellow-300 font-bold">PHP</span> and trying new things and technologies from time to time. <br />
                        Consider myself a responsible, prompt, diligent and optimistic person.
                    </p>
                    <p class="text-base dark:text-gray-400 mb-8">
                        Before started working as a developer, worked as a QA engineer for 6+ years. <br />
                    </p>
                    <p class="text-base dark:text-gray-400 mb-8">
                        Team player with great international work experience, communication with colleagues and managers from overseas.  <br />
                    </p>
                    <p class="text-base dark:text-gray-400 mb-8">
                        Working fully remote for the past 4 years. <br />
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ====== About Section End -->
