<!-- ====== Contact Section Start -->
<section id="contact" class="bg-white dark:bg-primary py-20 lg:py-[120px] overflow-hidden relative z-10">
    <div class="container">
        <div class="flex flex-wrap lg:justify-between -mx-4">
            <div class="w-full lg:w-1/2 xl:w-6/12 px-4">
                <div class="max-w-[570px] mb-12 lg:mb-0">
                    <h2 class="text-dark dark:text-slate-100 mb-6 uppercase font-bold text-[32px] sm:text-[40px] lg:text-[36px] xl:text-[40px]">
                        FOR BUSINESS ENQUIRIES
                    </h2>
                    <p class="text-base text-body-color dark:text-slate-200 leading-relaxed mb-9">
                        If you feel my experience and skills fits your needs, feel free to contact me.
                    </p>
                    <div class="flex mb-8 max-w-[370px] w-full">
                        <div
                            class="
                 max-w-[60px]
                 sm:max-w-[70px]
                 w-full
                 h-[60px]
                 sm:h-[70px]
                 flex
                 items-center
                 justify-center
                 mr-6
                 overflow-hidden
                 bg-primary bg-opacity-5
                 text-primary
                 dark:text-slate-300
                 rounded
                 "
                        >
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-full lg:w-1/2 xl:w-5/12 px-4">
                <div class="bg-white dark:bg-slate-900 relative rounded-lg p-8 sm:p-12 shadow-lg">
                    <form action="/contact/submit" method="POST" x-data="
          {
              formData: {
                name: '',
                email: '',
                message: '',
              },
              errors: {},
              successMessage: '',

              submitForm(event) {
                this.successMessage = '';
                this.errors = {};
                  fetch(`/contact/submit`, {
                    method: 'POST',
                    headers: {
                      'Content-Type': 'application/json',
                      'X-Requested-With': 'XMLHttpRequest',
                      'X-CSRF-TOKEN': document.querySelector(`meta[name='csrf-token']`).getAttribute('content')
                    },
                    body: JSON.stringify(this.formData)
                  })
                  .then(response => {
                    if (response.status === 200) {
                      return response.json();
                    }
                    throw response;
                  })
                  .then(result => {
                    this.formData = {
                      name: '',
                      email: '',
                      message: '',
                    };
                    this.successMessage = 'Thanks for your contact request. I will get back to you shortly.';
                  })
                  .catch(async (response) => {
                    const res = await response.json();
                    if (response.status === 422) {
                      this.errors = res.errors;
                    }
                    console.log(res);
                  })
              }
          }
          " x-on:submit.prevent="submitForm">
                        <template x-if="successMessage">
                            <div x-text="successMessage" class="py-4 px-6 bg-green-600 text-slate-100 mb-4"></div>
                        </template>
                        @csrf
                        <div class="mb-6">
                            <x-forms.input placeholder="Your Name" name="name" x-model="formData.name"
                                           ::class="errors.name ? 'border-red-500 focus:border-red-500' : ''"></x-forms.input>
                            <template x-if="errors.name">
                                <div x-text="errors.name[0]" class="text-red-500"></div>
                            </template>
                        </div>
                        <div class="mb-6">
                            <x-forms.input type="email" placeholder="Your Email" name="email" x-model="formData.email"
                                           ::class="errors.email ? 'border-red-500 focus:border-red-500' : ''"></x-forms.input>
                            <template x-if="errors.email">
                                <div x-text="errors.email[0]" class="text-red-500"></div>
                            </template>
                        </div>
                        <div class="mb-6">
                            <x-forms.textarea placeholder="Your Message" name="message" rows="6"
                                              x-model="formData.message"
                                              ::class="errors.message ? 'border-red-500 focus:border-red-500' : ''"></x-forms.textarea>
                            <template x-if="errors.message">
                                <div x-text="errors.message[0]" class="text-red-500"></div>
                            </template>
                        </div>
                        <div>
                            <x-button class="w-full">
                                Send Message
                            </x-button>
                        </div>
                    </form>
                    <div>
            <span class="absolute -top-10 -right-9 z-[-1]">
               <svg
                   width="100"
                   height="100"
                   viewBox="0 0 100 100"
                   fill="none"
                   xmlns="http://www.w3.org/2000/svg"
               >
                  <path
                      fill-rule="evenodd"
                      clip-rule="evenodd"
                      d="M0 100C0 44.7715 0 0 0 0C55.2285 0 100 44.7715 100 100C100 100 100 100 0 100Z"
                      fill="#3056D3"
                  />
               </svg>
            </span>
                        <x-contact-dots-top></x-contact-dots-top>
                        <x-contact-dots-bottom></x-contact-dots-bottom>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ====== Contact Section End -->
